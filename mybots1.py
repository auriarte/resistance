import random

from player import Bot
from aigd import Statistician
from aigd import LogicalBot
import itertools
import logging
import collections

class Loco(Bot):   
    def select(self, players, count):
        """Pick a sub-group of players to go on the next mission."""
        return random.sample(players, count)

    def vote(self, team):
        """Given a selected team, decide whether the mission should proceed.""" 
        return random.choice([True, False])

    def sabotage(self):
        """Decide what to do on the mission once it has been approved."""
        return random.choice([True, False])

'''
class EStatistician(Statistician):    
    def select(self, players, count):
        if self.spy and random.random() < (self.game.wins+1)/4:
            team = [random.choice(self.spies)]
            team += random.sample([i for i in players if i not in team],count-1)
        elif self.spy:
            return random.sample(players, count)
        else:
            team = [p for p in players if p.index == self.index]
            while len(team) < count:
                candidates = [p for p in players if p not in team]
                team.append(self._roulette(zip(candidates, [1.0 - self._estimate(p) for p in candidates])))
            return team
'''
'''
class StatisticianL(Statistician):
    def onGameComplete(self, win, spies):
        Statistician.onGameComplete(self, win, spies)
        self.log.debug(str(self.global_statistics))
'''
'''                
class CStatistician(Bot):

    global_statistics = {}

    def onGameRevealed(self, players, spies):
        self.spies = spies
        self.players = players
        
        self.proposed_teams = []
        self.completed_missions = []
        self.votes = []
        self.player_suspicion = {}
        self.player_suspicion_global = {}
        for player in players:
            self.player_suspicion[player] = 0
            self.player_suspicion_global[player] = 0
    
    def select(self, players, count):
        if self.spy:
            team = [random.choice(list(self.spies))]
            team += random.sample([i for i in players if i not in team],count-1)
        else:
            team = random.sample(players,count)
        return team

    def vote(self, team):
        # Store this for later once we know the spies.
        self.proposed_teams.append((self.game.leader, team))

        # Hard coded if spy, could use statistics to check what to do best!
        if self.spy:
            return len([p for p in team if p in self.spies]) > 0

        return random.choice([True,False])
        
    def sabotage(self):
        return self.spy
    
    def onMissionComplete(self, sabotaged):
        # Store this information for later once we know the spies.
        self.completed_missions.append((self.game.team.copy(), sabotaged))
        if sabotaged:
            for player in self.game.team:
                self.player_suspicion_global[player] += 1.0/len(self.game.team)
                if not self.spy and self in self.game.team and not self == player:
                    if len(self.game.team)==2:
                        self.player_suspicion[player] = 100
                    else:
                        self.player_suspicion[player] += 1.0/(len(self.game.team)-1)
                    
    def onVoteComplete(self, votes):
        # Store.
        self.votes.append((votes, self.game.team.copy()))
    def onGameComplete(self, win, spies):
        self.log.debug('---')
        self.log.debug(self)
        self.log.debug(spies)
        self.log.debug(self.player_suspicion)
        self.log.debug(self.player_suspicion_global)
'''        
class GarboA(Bot):
    def onGameRevealed(self, players, spies):
        self.spies = spies
        self.players = players
        self.suspicion = {}
        self.missions_done = {}
        for player in players:
            self.suspicion[player]=0
            self.missions_done[player]=0
        self.me = [p for p in players if p.index == self.index][0]
        self.others = [p for p in players if not p.index == self.index]
        
        self.log2 = self.log
        def nolog(params):
            pass
        if self.spy:
            self.log.debug = nolog
        self.log.debug("---GAME %s---" % str(self.me))
        self.log.debug(spies)
    def select(self, players, count):
        if(self.spy):
            # spy with lowest suspicion + resistance with lowest suspicion
            return [sorted(list(self.spies),key=lambda i:self.suspicion[i])[0]] + sorted([i for i in players if i not in self.spies],key=lambda i:self.suspicion[i])[0:count-1]
        else:
            # if we have to pick 3 players, we should include ourselves to minimize having a spy
            # if we are among the least suspicious players, we include ourselves
            if count > 2 or (self.me in sorted(self.suspicion.items(),key=lambda i:i[1])[0:2]):
                # myself + the least supicious of the remaining players 
                return [self.me] + sorted(self.others,key=lambda i:self.suspicion[i])[0:count-1]
            else:
                # least suspicious players 
                return sorted(self.game.players,key=lambda i:self.suspicion[i])[0:count]

    def onMissionComplete(self, sabotaged):
        # fact spy = 1000
        # fact res= -1000
        # suspicion ~100
        team = self.game.team
        no_team = [p for p in self.game.players if p not in self.game.team]
        for p in self.game.team:
            self.missions_done[p] += 1
        if sabotaged:
            # the team is suspicious
            if self.me not in self.game.team:
                if len(team)==2 and sabotaged==2:            
                    for p in team:
                        self.suspicion[p] += 1000
                    for p in no_team:
                        self.suspicion[p] -= 1000
                if len(team)==2 and sabotaged==1:
                    for p in team:
                        self.suspicion[p] += 50
                if len(team)==3 and sabotaged==2:
                    for p in team:
                        self.suspicion[p] += 100
                    for p in no_team:
                        self.suspicion[p] -= 1000
                if len(team)==3 and sabotaged==1:
                    for p in team:
                        self.suspicion[p] += 25
            else:
                if len(team)==2 and sabotaged==1:
                    for p in [p for p in team if not p==self.me]:
                        self.suspicion[p] += 1000
                    self.suspicion[self.me] += 50
                if len(team)==3 and sabotaged==2:
                    for p in [p for p in team if not p==self.me]:
                        self.suspicion[p] += 1000
                    self.suspicion[self.me] += 100
                    for p in no_team:
                        self.suspicion[p] -= 1000
                if len(team)==3 and sabotaged==1:
                    for p in [p for p in team if not p==self.me]:
                        self.suspicion[p] += 50
                    self.suspicion[self.me] += 25
            # the leader and voters are suspicious
            self.voterPenalites(team)
        else:
            if len(team)==2:
                for p in team:
                    self.suspicion[p] -= 50
            else:
                for p in team:
                    self.suspicion[p] -= 25   
        self.log.debug("---ROUND %d %s ---" % (self.game.turn,str(self.me)))
        self.log.debug(sabotaged)
        self.log.debug(self.game.team)
        self.log.debug(self.suspicion)
    def voterPenalites(self,team):
            if self.game.turn>1 and max([self.suspicion[i] for i in team])>=100:
                self.suspicion[self.game.leader] += 40
                for p in filter(lambda i:self.last_votes[i.index-1] and not i==self.game.leader, self.game.players):
                    self.suspicion[p]+=20
            if len(team)==3 and max([self.suspicion[i] for i in team])>=50:
                for i in [i for i in self.players if i not in team and self.last_votes[i.index-1]]:
                    self.suspicion[i] += 700 
    def onGameComplete(self, win, spies):
        self.log.debug("---END %s ---" % str(self.me))
        self.log.debug(self)
        self.log.debug(spies)
        self.log.debug(self.suspicion) 
    def sabotage(self):
        return self.spy 
    def vote(self, team):
        self.log.debug("---VOTING %d %s ---" % (self.game.tries,str(self.me)))
        self.log.debug(self.game.leader)
        self.log.debug(team)
        if self.game.tries == 5: return True
        suspects = sorted(self.game.players,key=lambda i:self.suspicion[i])
        suspects2 = [p for p in suspects if not p==self.me]
        if self.spy:
            return len([p for p in team if p in self.spies])>0
        else:
            # We need data
            if self.game.turn == 1: return True 
            if suspects[-1] in team: return False
            if len(team)==2:
                if self.me in team and suspects[-2] in team: return False    
            elif len(team)==3:
                # There is going to be a spy if it is not all resistance members, we want in
                if self.me not in team:
                    return False
                elif suspects2[-1] in team or suspects[-2] in team:
                    return False
        return True  
    def onVoteComplete(self, votes):
        our = votes[self.index-1]
        if self.game.turn>1 and self.game.tries<5:
            for i in self.game.players:
                if not votes[i.index-1]==our:
                    self.suspicion[i]+=8
            team = self.game.team
            team_filtered = filter(lambda i:self.suspicion[i]<750,team)
            if self.me not in team and len(team)==3 and len(team_filtered)<3:              
                not_team_voted_true = [i for i in self.game.players if i not in team and votes[i.index-1]]
                for i in not_team_voted_true:
                    self.suspicion[i]+=500
            if self.me not in team and len(team)==3:
                for i in [i for i in self.game.players if i not in team and votes[i.index-1]]:
                    self.suspicion[i]+=400
            # When there there is a spy NOT in the team, voting AGAINST the team, let's assume there are no spies on the team
            if len(filter(lambda i:self.suspicion[i]>750 and i not in team and not votes[i.index-1],self.game.players))>0:
                for i in self.game.team:  
                    self.suspicion[i]-=600
            
        self.last_votes = votes
        self.log.debug("---VOTES %s ---" % str(self.me))
        self.log.debug(votes)
''' 
class GarboB(GarboA):
    def onGameRevealed(self, players, spies):
        GarboA.onGameRevealed(self,players,spies)
        def nolog(param):
            pass
        self.log.debug = nolog 
    def voterPenalites(self,team):
            if self.game.turn>1 and sorted([self.suspicion[i] for i in team])[-1]>=75:
                self.suspicion[self.game.leader] += 40
                for p in filter(lambda i:self.last_votes[i.index-1] and not i==self.game.leader, self.game.players):
                    self.suspicion[p]+=20   
class GarboC(GarboA):
    def onGameRevealed(self, players, spies):
        GarboA.onGameRevealed(self,players,spies)
        def nolog(param):
            pass
        self.log.debug = nolog 
    def voterPenalites(self,team):
            if self.game.turn>1 and sorted([self.suspicion[i] for i in team])[-1]>=100:
                self.suspicion[self.game.leader] += 40
                for p in filter(lambda i:self.last_votes[i.index-1] and not i==self.game.leader, self.game.players):
                    self.suspicion[p]+=20    
class GarboD(GarboA):
    def onGameRevealed(self, players, spies):
        GarboA.onGameRevealed(self,players,spies)
        def nolog(param):
            pass
        self.log.debug = nolog 
    def voterPenalites(self,team):
            if self.game.turn>1 and sum([self.suspicion[i] for i in team])>=75:
                self.suspicion[self.game.leader] += 40
                for p in filter(lambda i:self.last_votes[i.index-1] and not i==self.game.leader, self.game.players):
                    self.suspicion[p]+=20               
'''

best_params = {}
num_runs = 0
        
class GarboP(Bot):
    def getSettings(self,spy):
        self.params = [1000,1000,50,100,1000,25,1000,50,1000,100,1000,50,25,50,25,40,20,700,8,800,400,600]
        self.thresholds = [100,750,750]
        self.select_include_self = 2
        # ML only when playing as res
        global best_params
        #self.best_params = best_params
        if not best_params:
            #setup
            best_params = dict([(i,[1,1,0.5]) for i in apply(itertools.product,[[75,100,125,250,500],[100,250,750,1000],[1,2,3],[100,250,500,1000],[100,250,500,750,1000]])])
            #99,150,499,750,999,1500
            self.log.info(best_params.keys())
            self.log.setLevel(logging.INFO)
        if not spy:
            if random.random() < 0.20:
                self.current_params = random.choice(best_params.keys())
            else:
                self.current_params = sorted(best_params.items(),key=lambda i:i[1][0]/i[1][1])[-1][0]
            best_params[self.current_params][1]+=1
            self.thresholds[0] = self.current_params[0]
            self.thresholds[1] = self.thresholds[2] = self.current_params[1]
            #facts
            self.params[0] = self.params[1] = self.params[4] = self.params[6] = self.params[8] = self.params[10] = self.current_params[3]
            #quasi facts
            self.params[17] = self.params[19] = self.params[20] = self.params[21] = self.current_params[4]
            #self exclusion threshold
            self.select_include_self = self.current_params[3]
    def onGameRevealed(self, players, spies):
        self.getSettings(self.spy)
        self.spies = spies
        self.players = players
        self.suspicion = {}
        self.missions_done = {}
        for player in players:
            self.suspicion[player]=0
            self.missions_done[player]=0
        self.me = [p for p in players if p.index == self.index][0]
        self.others = [p for p in players if not p.index == self.index]
        
        self.log2 = self.log
        def nolog(params):
            pass
        if self.spy:
            self.log.debug = nolog
        self.log.debug("---GAME %s---" % str(self.me))
        self.log.debug(spies)
    def select(self, players, count):
        if(self.spy):
            # spy with lowest suspicion + resistance with lowest suspicion
            return [sorted(list(self.spies),key=lambda i:self.suspicion[i])[0]] + sorted([i for i in players if i not in self.spies],key=lambda i:self.suspicion[i])[0:count-1]
        else:
            # if we have to pick 3 players, we should include ourselves to minimize having a spy
            # if we are among the least suspicious players, we include ourselves
            if count > 2 or (self.me in sorted(self.suspicion.items(),key=lambda i:i[1])[0:self.select_include_self]):
                # myself + the least supicious of the remaining players 
                return [self.me] + sorted(self.others,key=lambda i:self.suspicion[i])[0:count-1]
            else:
                # least suspicious players 
                return sorted(self.game.players,key=lambda i:self.suspicion[i])[0:count]
  
    def onMissionComplete(self, sabotaged):
        # fact spy = 1000
        # fact res= -1000
        # suspicion ~100
        team = self.game.team
        no_team = [p for p in self.game.players if p not in self.game.team]
        for p in self.game.team:
            self.missions_done[p] += 1
        if sabotaged:
            # the team is suspicious
            if self.me not in self.game.team:
                if len(team)==2 and sabotaged==2:            
                    for p in team:
                        self.suspicion[p] += self.params[0]
                    for p in no_team:
                        self.suspicion[p] -= self.params[1]
                if len(team)==2 and sabotaged==1:
                    for p in team:
                        self.suspicion[p] += self.params[2]
                if len(team)==3 and sabotaged==2:
                    for p in team:
                        self.suspicion[p] += self.params[3]
                    for p in no_team:
                        self.suspicion[p] -= self.params[4]
                if len(team)==3 and sabotaged==1:
                    for p in team:
                        self.suspicion[p] += self.params[5]
            else:
                if len(team)==2 and sabotaged==1:
                    for p in [p for p in team if not p==self.me]:
                        self.suspicion[p] += self.params[6]
                    self.suspicion[self.me] += self.params[7]
                if len(team)==3 and sabotaged==2:
                    for p in [p for p in team if not p==self.me]:
                        self.suspicion[p] += self.params[8]
                    self.suspicion[self.me] += self.params[9]
                    for p in no_team:
                        self.suspicion[p] -= self.params[10]
                if len(team)==3 and sabotaged==1:
                    for p in [p for p in team if not p==self.me]:
                        self.suspicion[p] += self.params[11]
                    self.suspicion[self.me] += self.params[12]
            # the leader and voters are suspicious
            self.voterPenalites(team)
        else:
            if len(team)==2:
                for p in team:
                    self.suspicion[p] -= self.params[13]
            else:
                for p in team:
                    self.suspicion[p] -= self.params[14]
        self.log.debug("---ROUND %d %s ---" % (self.game.turn,str(self.me)))
        self.log.debug(sabotaged)
        self.log.debug(self.game.team)
        self.log.debug(self.suspicion)
    def voterPenalites(self,team):
            if self.game.turn>1 and max([self.suspicion[i] for i in team])>=self.thresholds[0]:
                self.suspicion[self.game.leader] += self.params[15]
                for p in filter(lambda i:self.last_votes[i.index-1] and not i==self.game.leader, self.game.players):
                    self.suspicion[p]+=self.params[16]
            if len(team)==3 and max([self.suspicion[i] for i in team])>=50:
                for i in [i for i in self.players if i not in team and self.last_votes[i.index-1]]:
                    self.suspicion[i] += self.params[17]
    def onGameComplete(self, win, spies):
        self.log.debug("---END %s ---" % str(self.me))
        self.log.debug(self)
        self.log.debug(spies)
        self.log.debug(self.suspicion) 
        global best_params
        global num_runs
        num_runs+=1
        if win and not self.spy:
            try:
                best_params[self.current_params][0]+=1
                best_params[self.current_params][2] += sum([(1 if self.suspicion[j]>= 1000 else 0.5) for j in [i for i in sorted(self.game.players,key=lambda i:self.suspicion[i],reverse=True)[0:len(spies)] if i in spies]])/len(spies)
                best_params[self.current_params][2] /= 2
            except:
                pass
        if not self.spy and num_runs%100==0:
            self.log.info(",".join(["%.2f"%(1.0*i[0]/i[1]) for i in best_params.values()]))
        if not self.spy and num_runs%1000==0:
            self.log.info(best_params)
    def sabotage(self):
        return self.spy 
    def vote(self, team):
        self.log.debug("---VOTING %d %s ---" % (self.game.tries,str(self.me)))
        self.log.debug(self.game.leader)
        self.log.debug(team)
        if self.game.tries == 5: return True
        suspects = sorted(self.game.players,key=lambda i:self.suspicion[i])
        suspects2 = [p for p in suspects if not p==self.me]
        if self.spy:
            return len([p for p in team if p in self.spies])>0
        else:
            # We need data
            if self.game.turn == 1: return True 
            if suspects[-1] in team: return False
            if len(team)==2:
                if self.me in team and suspects[-2] in team: return False    
            elif len(team)==3:
                # There is going to be a spy if it is not all resistance members, we want in
                if self.me not in team:
                    return False
                elif suspects2[-1] in team or suspects[-2] in team:
                    return False
        return True  
    def onVoteComplete(self, votes):
        our = votes[self.index-1]
        if self.game.turn>1 and self.game.tries<5:
            for i in self.game.players:
                if not votes[i.index-1]==our:
                    self.suspicion[i]+=self.params[18]
            team = self.game.team
            team_filtered = filter(lambda i:self.suspicion[i]<self.thresholds[1],team)
            if self.me not in team and len(team)==3 and len(team_filtered)<3:              
                not_team_voted_true = [i for i in self.game.players if i not in team and votes[i.index-1]]
                for i in not_team_voted_true:
                    self.suspicion[i]+=self.params[19]
            if self.me not in team and len(team)==3:
                for i in [i for i in self.game.players if i not in team and votes[i.index-1]]:
                    self.suspicion[i]+=self.params[20]
            # When there is a spy NOT in the team, voting AGAINST the team, let's assume there are no spies on the team
            if len(filter(lambda i:self.suspicion[i]>self.thresholds[2] and i not in team and not votes[i.index-1],self.game.players))>0:
                for i in self.game.team:  
                    self.suspicion[i]-=self.params[21]
            
        self.last_votes = votes
        self.log.debug("---VOTES %s ---" % str(self.me))
        self.log.debug(votes)
'''        
class GarboQ(GarboP):
    def getSettings(self,spy):
        self.params = [1000,1000,50,100,1000,25,1000,50,1000,100,1000,50,25,50,25,40,20,700,8,800,400,600]
        self.thresholds = [100,750,750]
        self.select_include_self = 2
        # ML only when playing as res
        global best_params
        #self.best_params = best_params
        if not best_params:
            #setup
            best_params = dict([(i,[1,1,0.5]) for i in apply(itertools.product,[[75,100,125,250,500],[100,250,750,1000],[1,2,3],[100,250,500,1000],[250,500,750,1000]])])
            #99,150,499,750,999,1500
            self.log.info(best_params.keys())
            self.log.setLevel(logging.INFO)
        if not spy:
            if random.random() < 0.20:
                self.current_params = random.choice(best_params.keys())
            else:
                self.current_params = sorted(best_params.items(),key=lambda i:i[1][2])[-1][0]
            best_params[self.current_params][1]+=1
            self.thresholds[0] = self.current_params[0]
            self.thresholds[1] = self.thresholds[2] = self.current_params[1]
            #facts
            self.params[0] = self.params[1] = self.params[4] = self.params[6] = self.params[8] = self.params[10] = self.current_params[3]
            #quasi facts
            self.params[17] = self.params[19] = self.params[20] = self.params[21] = self.current_params[4]
            #self exclusion threshold
            self.select_include_self = self.current_params[2]
'''
class GarboS(GarboP):
    def initSettings(self):
        self.current_params = (250, 100, 3, 1000, 1000)
    def getSettings(self,spy):  
        self.params = [1000,1000,50,100,1000,25,1000,50,1000,100,1000,50,25,50,25,40,20,700,8,800,400,600]
        self.thresholds = [100,750,750]
        self.select_include_self = 2
        self.initSettings()
        self.thresholds[0] = self.current_params[0]
        self.thresholds[1] = self.thresholds[2] = self.current_params[1]
        #facts
        self.params[0] = self.params[1] = self.params[4] = self.params[6] = self.params[8] = self.params[10] = self.current_params[3]
        #quasi facts
        self.params[17] = self.params[19] = self.params[20] = self.params[21] = self.current_params[4]
        #self exclusion threshold
        self.select_include_self = self.current_params[2]
class GarboS1(GarboS):
    def initSettings(self):  
        self.current_params = (250, 100, 3, 1000, 1000)
class GarboS2(GarboS):
    def initSettings(self):  
        self.current_params = (100, 100, 1, 250, 750)
class GarboS3(GarboS):
    def initSettings(self):  
        self.current_params = (75, 250, 2, 250, 750)
class GarboS4(GarboS):
    def initSettings(self):  
        self.current_params = (125, 100, 1, 250, 750)
class GarboS5(GarboS):
    def initSettings(self):  
        self.current_params = (75, 1000, 1, 1000, 1000)

times_spies_on_team = collections.defaultdict(lambda:0)

class GarboB1(GarboA):
    def sabotage(self):
        # if there are two spies in the team, don't sabotage
        spies_on_team = [p for p in self.game.team if p in self.spies]
        if len(spies_on_team)>1: 
            global times_spies_on_team
            times_spies_on_team[self.name]+=1
            print times_spies_on_team.items()
        if len(spies_on_team) > 1 and self.game.wins < 1:
            return False
        # otherwise sabotage
        return True
class GarboB2(GarboA):
    def sabotage(self):
        # if there are two spies in the team, make sure there is only one sabotage
        spies_on_team = [p for p in self.game.team if p in self.spies]
        if len(spies_on_team)>1: 
            global times_spies_on_team
            times_spies_on_team[self.name]+=1
            print times_spies_on_team.items()
        if len(spies_on_team) > 1 and list(self.spies)[0] in spies_on_team and self.me in list(self.spies)[1:]:
            return False
        # otherwise sabotage
        return True
class GarboB3(GarboA):
    def sabotage(self):
        # if there are two spies in the team, don't sabotage
        spies_on_team = [p for p in self.game.team if p in self.spies]
        if len(spies_on_team)>1: 
            global times_spies_on_team
            times_spies_on_team[self.name]+=1
        if len(spies_on_team) > 1 and random.random() < 0.5:
            return False
        # otherwise sabotage
        return True
